"use strict"

const usersUrl = "https://ajax.test-danit.com/api/json/users";

const postsUrl = "https://ajax.test-danit.com/api/json/posts";

class Card {
  name;
  email;
  userId;
  title;
  text;
  postId;
  constructor({
    name,
    email,
    userId,
    title,
    text,
    postId
  }) {
    this.name = name;
    this.email = email;
    this.userId = userId;
    this.title = title;
    this.text = text;
    this.postId = postId;

  }

  place() {
    const wrapper = document.querySelector(".wrapper");
    wrapper.insertAdjacentHTML('beforeend',
      `
      <div data-user-id='${this.userId}' data-post-id='${this.postId}' class="card" id="card${this.postId}">
      <div class="title-box">
        <h2 class="user-name">${this.name}</h2>
        <a href="mailto:${this.email}" class="user-email">${this.email}</a>
        <button onclick="deleteCard(${this.postId})" type="button" class="btn btn-close"></button></div>
        <div class="text-box">
        <h3>${this.title}</h3>
          <p class="text-success">
            ${this.text}
          </p>
        </div>
    </div>
        `)
  }
}

async function getInfo() {
  const responseUser = await fetch(usersUrl);
  const resultUser = await responseUser.json()

  const responsePosts = await fetch(postsUrl);
  const resultPosts = await responsePosts.json()

  console.log(resultUser);
  console.log(resultPosts);


  makeCards(resultPosts, resultUser)
}

getInfo()

function makeCards(posts, users) {
  posts.forEach(post => {
    if (!!post.userId) {
      const usersCard = users.find(user => user.id === post.userId)
      if (usersCard.id === post.userId) {
        const objCard = {
          name: usersCard.name,
          email: usersCard.email,
          title: post.title,
          text: post.body,
          postId: post.id,
          userId: usersCard.id,
        }
        const card = new Card(objCard);
        card.place()
      }
    }
  });
}


async function deleteCard(postId) {
  const res = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
  });

  document.querySelector(`#card${postId}`).remove()


}