"use strict";

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування в JavaScript - це механізм, який дозволяє створювати нові об'єкти на основі інших об'єктів, називаних "прототипами".

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Щоб скопіювати властивості батьківського класу

class Employee {
  #name;
  #age;
  #salary;
  constructor(name, age, salary) {
    this.#name = name;
    this.#age = age;
    this.#salary = salary;
  }
  set name(value) {
    this.#name = value;
  }
  get name() {
    return this.#name;
  }
  set age(value) {
    this.#age = value;
  }
  get age() {
    return this.#age;
  }
  set salary(value) {
    this.#salary = value;
  }
  get salary() {
    return this.#salary;
  }
}

class Programmer extends Employee {
  #lang;
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.#lang = lang;
  }
  set lang(value) {
    this.#lang = value;
  }
  get lang() {
    return this.#lang;
  }
  get salary() {
    return super.salary * 3;
  }
}

const programmer1 = new Programmer("Oleh", 20, 2000, "EN");
const programmer2 = new Programmer("Petro", 22, 3000, "UKR");
const programmer3 = new Programmer("Vasya", 25, 1500, "RU");
// console.log(programmer1);
// console.log(programmer1.salary);
// console.log(programmer2);
// console.log(programmer2.salary);
// console.log(programmer3);
// console.log(programmer3.salary);
